Title: Virtual screen
Summary: Expand your desktop with another device.
Date: 2021-01-09
Category: Tech
Tags: android, linux, vnc, x11

Ever wondered if you could drag a window from your PC to a phone or tablet
standing on your desk? It turns out you can!

I wrote this guide, because I've only found bits and pieces around, with many
things being outdated or not working.



## How to

### 1) Create a virtual screen

Install [evdi](https://github.com/DisplayLink/evdi/) kernel module

    git clone https://github.com/DisplayLink/evdi.git
    cd evdi/module
    
    make && sudo make install
    # or
    sudo make install_dkms

Add `evdi` to `/etc/modules-load.d/local.conf`.

Add `options evdi initial_device_count=1` to `/etc/modprobe.d/local.conf`.

WARNING! This next step will freeze your Xorg, so you might want to reboot
instead or be clever about killing your Xorg or something.

    sudo modprobe evdi


### 2) Set a resolution and position to your virtual screen

    w=1280
    h=720
    
    set -- $(cvt "$w" "$h" 60 | grep Modeline)
    name="${2#\"}"
    name="${name%\"}"
    shift 2
    
    xrandr --newmode "$name" "$@"
    xrandr --addmode DVI-I-1-1 "$name"
    xrandr \
        --output DVI-I-1-1 \
        --mode "$name" \
        --left-of eDP-1


### 3) Share your virtual screen with VNC

    geometry="$(xrandr | grep DVI-I-1-1 | cut -d" " -f 3)"
    x0vncserver \
        -display :0 \
        -SecurityTypes none \
        -Geometry "$geometry"
    # or
    x11vnc \
        -display :0 \
        -clip "$geometry" \
        -forever

For `x0vncserver` instead of `-SecurityTypes none` you might want to use
`-PasswordFile` option instead. Consult your `man` page.


### 4) (optional) Attach Android device through USB cable

    adb reverse tcp:5900 tcp:5900

Now you can point your VNC client application (like [MultiVNC](https://f-droid.org/packages/com.coboltforge.dontmind.multivnc/)) to `localhost:5900`.



## What is wrong in other guides


### Using obsolete `intel` driver

Intel driver has `Option "VirtualHeads"`, but you really want to stick to default `modesetting` driver.


### Using `dummy` driver

According to ArchLinux wiki page
"[Extreme_Multihead#VNC](https://wiki.archlinux.org/index.php/Extreme_Multihead#VNC)"

> It is in principle possible to set up a dummy device alongside a real device
by adding a dummy device and xinerama configuration to xorg. It is tricky to
have this configuration working properly, and it is not ideal for laptops, as
you won't be able to hotplug monitors.



### Using `xrandr --fb` to enlarge screen size

You can't make a window go fullscreen or snap to the edges.



### Using an unused output

It fails for me. I have a `DP-1` in `xrandr`, but no physical DisplayPort output.

    $ xrandr --output DP-1 \
        --mode 1280x720_60.00 \
        --left-of eDP-1
    xrandr: Configure crtc 2 failed



## References

- [https://askubuntu.com/questions/28608/how-do-you-use-an-android-tablet-as-a-second-display](https://askubuntu.com/questions/28608/how-do-you-use-an-android-tablet-as-a-second-display)
- [https://unix.stackexchange.com/questions/585069/unable-to-add-a-virtual-dispplay-to-xorg/585078#585078](https://unix.stackexchange.com/questions/585069/unable-to-add-a-virtual-dispplay-to-xorg/585078#585078)
- [https://github.com/brunodles/linux-second-screen/blob/master/tutorial.md](https://github.com/brunodles/linux-second-screen/blob/master/tutorial.md)
- [https://wiki.archlinux.org/index.php/Extreme_Multihead](https://wiki.archlinux.org/index.php/Extreme_Multihead)
