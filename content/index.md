title: Adam "Etam" Mizerski
save_as: index.html
status: hidden

I am:

- Hacker
- Free Software enthusiast
- GNU/Linux user
- Software developer (using mostly C++ and Rust)
- Sysadmin

Interested in:

- Decentralized and distributed software
- Selfhosting
- Security and privacy
