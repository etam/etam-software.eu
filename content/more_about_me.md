title: More about me

## My computer setup

I'm using [openSUSE](https://opensuse.org/) on my computers and servers for
daily use, gaming and working. Sometimes I'm involved in 
[packaging](https://build.opensuse.org/users/etamPL) stuff I need or like.

My desktop environment is KDE session with i3 window manager, disabled Plasma
desktop and systemtray plasmoid running as a window hidden in scratchpad. This
allows me to have i3 tiling window manager, but with KRunner and other hard
stuff (system tray, global shortcuts, removable media mounting) handled by KDE.

For coding big things I use Emacs and for small things mcedit. I use
[Kinesis Advantage 2](https://kinesis-ergo.com/keyboards/advantage2-keyboard/)
keyboard with [Programmer Dvorak](https://www.kaufmann.no/roland/dvorak/)
layout.


## My CV

- [pl]({static}/static/cv_pl.pdf)
- [en]({static}/static/cv_en.pdf)


## My nickname

"Etam" is a fictional name from a Polish fable book
["13 bajek z królestwa Lailonii"](https://pl.wikipedia.org/wiki/13_bajek_z_kr%C3%B3lestwa_Lailonii_dla_du%C5%BCych_i_ma%C5%82ych#Czerwona_%C5%82ata).

It sounds identical to "e tam!", which according to dictionaries
([ling.pl](https://ling.pl/slownik/polsko-angielski/e%20tam!), [bab.la](https://pl.bab.la/slownik/polski-angielski/e-tam))
is translated to "ah", "oh" or "you’re kidding".


## My avatar

My avatar comes from "VG Cats" (or "Videogame Cats") webcomic, from strip
[#226 "Smooth moves"](https://web.archive.org/web/20200708020302/https://www.vgcats.com/comics/?strip_id=225).

The author gave permission to create and use avatars, by creating some by
himself and writing
["Premade avatars from the comic. You're welcome to make your own as well."](https://web.archive.org/web/20200505023248/https://www.vgcats.com/misc/).
