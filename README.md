# [etam-software.eu](https://etam-software.eu)

## Rules

1. Thou shalt not produce unnecessarily mangled or cluttered output.
2. Thou shalt not include JavaScript.
3. Thou shalt not load external resources.
